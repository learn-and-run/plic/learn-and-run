// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "ERoomState.generated.h"

UENUM(BlueprintType)
enum class ERoomState : uint8
{
    Pending             UMETA(DisplayName = "Pending"),
    Running             UMETA(DisplayName = "Running"),
    GoodAnswerGiven     UMETA(DisplayName = "GoodAnswerGiven"),
    BadAnswerGiven      UMETA(DisplayName = "BadAnswerGiven"),
    Over                UMETA(DisplayName = "Over")
};
