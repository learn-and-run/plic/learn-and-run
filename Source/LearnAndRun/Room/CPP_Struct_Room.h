// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Struct_Room.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Room
{
	GENERATED_USTRUCT_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	int id;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	FString name = FString();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Answer")
	FString question = FString();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Answer")
	FText goodAnswer = FText();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Answer")
	FText badAnswer = FText();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clues")
	TArray<FText> clues = TArray<FText>();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clues")
	FString tip = FString();
};
