// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ERoomState.h"
#include "CPP_MainRoom.generated.h"

UCLASS()
class LEARNANDRUN_API ACPP_MainRoom : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ACPP_MainRoom();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	bool _roomFinished = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	bool _roomSucceed = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Circuit")
	bool _goToNextRoom = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	ERoomState state = ERoomState::Pending;
};
