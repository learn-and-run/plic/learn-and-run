// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.


#include "Http_Character.h"

// Sets default values
AHttp_Character::AHttp_Character()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHttp_Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHttp_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

