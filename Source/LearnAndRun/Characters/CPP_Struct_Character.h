// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "CPP_MainCharacter.h"
#include "CPP_Struct_Character.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Character
{
	GENERATED_USTRUCT_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	int id;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Picture")
	UObject* picture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprint")
	TSubclassOf<class ACPP_MainCharacter> characterActorClass;
};
