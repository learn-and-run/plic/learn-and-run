// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.


#include "CPP_MainCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "InputCoreTypes.h"

// Sets default values
ACPP_MainCharacter::ACPP_MainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ACPP_MainCharacter::PauseAction()
{
	UGameplayStatics::SetGamePaused(GetWorld(), !UGameplayStatics::IsGamePaused(GetWorld()));
}

// Called when the game starts or when spawned
void ACPP_MainCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACPP_MainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACPP_MainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	//PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &ACPP_MainCharacter::PauseAction).bExecuteWhenPaused = true;

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACPP_MainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACPP_MainCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Run", this, &ACPP_MainCharacter::Sprint);
}



void ACPP_MainCharacter::Sprint(float axis)
{
	float deltaSpeed = _runSpeed - _walkSpeed;
	float bonusSpeed = deltaSpeed * axis;
	float runSpeed = _walkSpeed + bonusSpeed;

	GetCharacterMovement()->MaxWalkSpeed = runSpeed;
}

void ACPP_MainCharacter::AddMovementToPlayer(const float& axis, EAxis::Type type)
{
	if (Controller != nullptr && axis != 0)
	{
		// get the yaw rotation
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get the direction vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(type);
		AddMovementInput(Direction, axis);
	}
}

void ACPP_MainCharacter::MoveRight(float axis)
{
	AddMovementToPlayer(axis, EAxis::Y);
}

void ACPP_MainCharacter::MoveForward(float axis)
{
	AddMovementToPlayer(axis, EAxis::X);
}
