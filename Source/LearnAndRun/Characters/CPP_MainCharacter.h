// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPP_MainCharacter.generated.h"

UCLASS()
class LEARNANDRUN_API ACPP_MainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPP_MainCharacter();

private:
	/// <summary>
	/// Called for making sprint the character.
	/// </summary>
	/// <param name="axis">The range of value between 0 and 1 that correspond to the axis input.</param>
	void Sprint(float axis = 0.0F);

	/// <summary>
	/// Called for moving forward/backward.
	/// </summary>
	/// <param name="axis">The range of value between -1 and 1 that correspond to the axis input.</param>
	void MoveForward(float axis = 0.0F);

	/// <summary>
	/// Called for moving to side.
	/// </summary>
	/// <param name="axis">The range of value between -1 and 1 that correspond to the axis input.</param>
	void MoveRight(float axis = 0.0F);

	/// <summary>
	/// Add movement to a player character.
	/// </summary>
	/// <param name="axis">The range of value between -1 and 1 that correspond to the axis input</param>
	/// <param name="type">The type that represent the axis on wich add the movement.</param>
	void AddMovementToPlayer(const float& axis, EAxis::Type type);

	/// <summary>
	/// Make the game in pause if it is not and bring it to no pause if it is.
	/// </summary>
	void PauseAction();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Movement")
	float _walkSpeed = 93.75F;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Movement")
	float _runSpeed = 375.0F;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
    bool _interactionIsPossible = true;
};
