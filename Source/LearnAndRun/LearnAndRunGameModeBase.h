// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LearnAndRunGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LEARNANDRUN_API ALearnAndRunGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
