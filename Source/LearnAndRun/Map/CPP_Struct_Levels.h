// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Struct_Levels.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Levels
{
	GENERATED_USTRUCT_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	FName mainMenu = FName(TEXT("Level_MainMenu"));
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	FName mainLevel = FName(TEXT("Level_MainGame"));
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	FName connection = FName(TEXT("Level_Connection"));
};
