// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Struct_Friend.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Friend
{
	GENERATED_USTRUCT_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	int relationId;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	FString pseudo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int score;
};
