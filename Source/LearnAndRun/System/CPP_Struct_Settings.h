// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Struct_Settings.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Settings
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
	bool isGamePad = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
	bool isDualshock = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
	bool isRetro = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	FString identity;
};
