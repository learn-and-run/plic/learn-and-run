// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CPP_GameMode_Character.generated.h"

/**
 * 
 */
UCLASS()
class LEARNANDRUN_API ACPP_GameMode_Character : public AGameModeBase
{
	GENERATED_BODY()
	
};
