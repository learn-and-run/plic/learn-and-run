// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_MainInteractableObject.generated.h"

UCLASS()
class LEARNANDRUN_API ACPP_MainInteractableObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_MainInteractableObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/// <summary>
	/// Display the pictogramme associated with the interaction.
	/// </summary>
	UFUNCTION(BlueprintCallable, Category = "MainInteractableObject")
		void DisplayInteractionPictogramme();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	bool _isFocused = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Answer")
	bool _isGoodAnswer = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Answer")
	bool _isBadAnswer = false;
};
