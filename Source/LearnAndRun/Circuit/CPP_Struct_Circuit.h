// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LearnAndRun/Room/CPP_Struct_Room.h"
#include "CPP_Struct_Circuit.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Struct_Circuit
{
	GENERATED_USTRUCT_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	int id;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	FText circuitName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Identity")
	UObject* picture;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int bestScore;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int currentScore;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int spendTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int bestTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms")
	int currentRoomIndex;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms")
	TArray<FCPP_Struct_Room> rooms;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms")
	TArray<FCPP_Struct_Room> failedRooms;
};
