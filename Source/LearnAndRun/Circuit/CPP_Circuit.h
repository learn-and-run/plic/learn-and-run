// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Circuit.generated.h"

UCLASS()
class LEARNANDRUN_API ACPP_Circuit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Circuit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameter")
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameter")
	int currentRoom = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameter")
	int numberRooms;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameter")
	int _score = 0;
};
