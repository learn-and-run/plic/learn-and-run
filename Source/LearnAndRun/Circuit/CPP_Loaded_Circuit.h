// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Struct_Circuit.h"
#include "EModule.h"
#include "CPP_Loaded_Circuit.generated.h"

USTRUCT(BlueprintType)
struct LEARNANDRUN_API FCPP_Loaded_Circuit
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module")
	EModule module;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Module")
	TArray<FCPP_Struct_Circuit> circuits;
};
