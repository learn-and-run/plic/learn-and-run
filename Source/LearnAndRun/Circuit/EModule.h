// Learn&Run project belongs to Learn&Run project group of the MTI major of EPITA. The team is composed by Océane MERLO, Nicolas ACART, Antonin Ginet and Baptiste LLORET. Do not copy this project.

#pragma once

#include "CoreMinimal.h"
#include "EModule.generated.h"

UENUM(BlueprintType)
enum class EModule : uint8
{
    GENERAL     UMETA(DisplayName = "General"),
    MATHS       UMETA(DisplayName = "Maths"),
    FRANCAIS    UMETA(DisplayName = "French"),
    CHIMIE      UMETA(DisplayName = "Chemistry"),
    PHYSIQUE    UMETA(DisplayName = "Physics"),
    ANGLAIS     UMETA(DisplayName = "English"),
    HISTOIRE    UMETA(DisplayName = "History"),
    GEOGRAPHIE  UMETA(DisplayName = "Geography"),
};
